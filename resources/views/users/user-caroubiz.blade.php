@extends('Layouts.user-layout')

@section('content')
    <div class="mt-[24px] p-[24px]
    border-[#f0f0f1] border-solid border-[1px]
    rounded-lg shadow-lg">
        <div class="flex mb-[8px] items-center">
            <img src="https://mweb-cdn.karousell.com/build/caroubiz-logo-Wz2oGHVn8L.svg"
            class="w-[24px] h-[24px] mr-[8px]">
            <p class="text-[30px] leading-[38px] font-medium">
                CarouBiz
            </p>
        </div>
        <p class="pt-[24px] text-[16px] leading-[24px] text-[#57585a]">
            List more general listings at a time with CarouBiz
            and unlock useful tools to attract more buyers,
            manage your store and grow your business.
        </p>
        <p class="pt-[24px] text-[16px] leading-[24px] text-[#57585a]">
            Try out video listings, quick and auto reply,
            custom collections and more with a free trial.
        </p>
        <button class="bg-[#ff2636] px-[16px] py-[8px]
        rounded-lg mt-[24px] text-white font-medium">
            Start 30-day free trial
        </button>
    </div>
@endsection

@extends('Layouts.user-layout')

@section('content')
    <div class="flex px-[16px] mt-[16px] items-center justify-between">
        <div class="w-[33.3%]">
            <p class="text-[18px] leading-[26px] text-[#57585a]">
                0
            </p>
            <p class="text-[18px] leading-[26px] font-bold text-[#2c2c2d]">
                Followers
            </p>
        </div>

        <div class="w-[33.3%]">
            <p class="text-[18px] leading-[26px] text-[#57585a]">
                0
            </p>
            <p class="text-[18px] leading-[26px] font-bold text-[#2c2c2d]">
                Following
            </p>
        </div>
    </div>
@endsection

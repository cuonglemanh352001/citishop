<div class="my-[32px] flex flex-col justify-center items-center
 mobile:w-full mobile:my-[16px]">
    <img src="https://mweb-cdn.karousell.com/build/no-review-JpfSAqrMkR.svg"
    class="w-[184px] h-[184px] mb-[24px]
    mobile:w-[128px] mobile:h-[128px] mobile:mb-[16px]">
    <p class="text-[16px] leading-[24px] font-medium text-[#57585a] mb-[8px]">
        {{$name}} has no reviews yet.
    </p>
    <p class="text-[16px] leading-[24px] text-center
     text-[#57585a] mb-[24px] w-[384px]">
        Reviews are given when a buyer or seller completes a deal.
        Chat with {{$name}} to find out more!
    </p>
</div>

<div class="py-[16px] px-[16px] w-full flex items-center justify-between
border-b-[1px] border-solid border-[#f0f0f1]">
    <div class="flex items-center justify-start">
        <p class="text-[20px] leading-[28px] font-medium">
            148
        </p>
        <img src="https://mweb-cdn.karousell.com/build/coin-2fj_L7WVuh.svg"
        class="w-[20px] h-[20px] mx-[4px]">
        <div class="ml-[8px] px-[8px] border-solid
        border-[1px] border-[#ff2636] h-[22px] rounded-full
        flex items-center justify-center">
            <p class="text-[12px] leading-[16px] text-[#ff2636]">
                Save 10%
            </p>
        </div>
    </div>


    <button class="px-[16px] py-[8px] bg-[#008f79] rounded-lg
     text-[16px] leading-[24px] font-medium text-white">
        SGD1.33
    </button>
</div>

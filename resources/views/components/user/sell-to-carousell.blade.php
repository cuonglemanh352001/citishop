<div class="mt-[160px] mobile:mt-0">
    <div class="flex
    mobile:flex-col-reverse">
        <p class="text-[#2c2c2d] text-[20px] leading-[28px] font-bold mr-[8px]">
            Sell to Carousell
        </p>
        <p class="text-[14px] leading-[22px] font-bold text-white
        px-[8px] bg-[#2c2c2d] rounded-lg pt-[2px]
        mobile:w-fit">
            Beta
        </p>
    </div>

    <div class="mb-[24px]">
        <p class="text-[#2c2c2d] text-[14px] leading-[22px]">
            No listed needed, sell directly to us
        </p>
    </div>

    <div class="grid grid-cols-2 p-[16px] shadow-lg gap-[20px] mobile:p-0
    border-[#f0f0f1] border-solid border-[1px] rounded-lg h-[296px]
    mobile:flex mobile:flex-col mobile:shadow-none mobile:border-none">
        <div class="flex items-center justify-center
        mobile:flex-row-reverse mobile:justify-between
        mobile:h-[78px] mobile:px-[16px]
        mobile:border-[1px] mobile:border-solid mobile:border-[#f0f0f1] mobile:rounded-lg">
            <img src="https://sl3-cdn.karousell.com/instant-sell/mobile_v2@2x.png"
            class="w-[128px] h-[128px] mobile:w-[76px] mobile:h-[76px]
            object-contain mobile:object-cover">
            <div>
                <p class="text-[#2c2c2d] text-[20px] leading-[28px] font-bold
                mobile:text-[18px]">
                    Mobile
                </p>
                <p class="text-[#2c2c2d] text-[14px] leading-[22px] pr-[10px]">
                    Cashout within 24h
                </p>
            </div>
        </div>

        <div class="flex items-center justify-center
        mobile:flex-row-reverse mobile:justify-between
        mobile:h-[78px]
        mobile:border-[#f0f1f1] mobile:border-solid mobile:border-[1px]
        mobile:px-[16px] mobile:rounded-lg">
            <img src="https://sl3-cdn.karousell.com/instant-sell/fashion_v2@2x.png"
                 class="w-[128px] h-[128px] mobile:w-[76px] mobile:h-[76px]
                 object-contain mobile:object-cover">
            <div>
                <p class="text-[#2c2c2d] text-[20px] leading-[28px] font-bold
                mobile:text-[16px]">
                    Fashion
                </p>
                <p class="text-[#2c2c2d] text-[14px] leading-[22px] pr-[10px]">
                    Clean out your closet
                </p>
            </div>
        </div>

        <div class="flex items-center justify-center
        mobile:flex-row-reverse mobile:justify-between
        mobile:h-[78px]
        mobile:border-[#f0f1f1] mobile:border-solid mobile:border-[1px]
        mobile:px-[16px] mobile:rounded-lg">
            <img src="https://sl3-cdn.karousell.com/instant-sell/luxury_bag_v2@2x.png"
                 class="w-[128px] h-[128px] mobile:w-[76px] mobile:h-[76px]
                 object-contain mobile:object-cover">
            <div>
                <p class="text-[#2c2c2d] text-[20px] leading-[28px] font-bold
                mobile:text-[18px]">
                    Luxury handbags
                </p>
                <p class="text-[#2c2c2d] text-[14px] leading-[22px] pr-[10px]">
                    Highest offer in the market
                </p>
            </div>
        </div>

        <div class="flex items-center justify-center
        mobile:flex-row-reverse mobile:justify-between
        mobile:h-[78px]
        mobile:border-[#f0f1f1] mobile:border-solid mobile:border-[1px]
        mobile:px-[16px] mobile:rounded-lg">
            <img src="https://sl3-cdn.karousell.com/instant-sell/car_v2@2x.png"
                 class="w-[128px] h-[128px] mobile:w-[76px] mobile:h-[76px]
                 object-contain mobile:object-cover">
            <div>
                <p class="text-[#2c2c2d] text-[20px] leading-[28px] font-bold
                mobile:text-[18px]">
                    Cars
                </p>
                <p class="text-[#2c2c2d] text-[14px] leading-[22px] pr-[10px]">
                    Get the best quote in 24h
                </p>
            </div>
        </div>
    </div>
</div>

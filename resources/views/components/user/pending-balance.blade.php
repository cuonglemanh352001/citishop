<!-- Modal -->
<div class="modal bg-[rgba(0,0,0,0.3)] fixed top-0 left-0 hidden w-full h-full outline-none overflow-x-hidden overflow-y-auto"
     id="pending-balance" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog relative w-auto pointer-events-none mt-[12%] max-w-[720px] w-[720px]
    mobile:w-full mobile:h-full mobile:mt-0 mobile:mx-0">
        <div
            class="modal-content border-none shadow-lg relative flex flex-col w-full pointer-events-auto bg-white bg-clip-padding rounded-md outline-none text-current
            mobile:w-full mobile:h-full">
            <div
                class="modal-header flex items-center border-b border-gray-200 rounded-t-md h-[64px] mobile:px-[16px]" >
                <button type="button"
                        class="w-[24px] h-[24px] mobile:mr-[80px] text-black border-none rounded-none opacity-50
                        focus:shadow-none focus:outline-none focus:opacity-100 hover:text-black hover:opacity-75 hover:no-underline"
                        data-bs-dismiss="modal" aria-label="Close">
                    <i class="fas fa-arrow-left fa-xl"></i>
                </button>
                <h5 class="text-[24px] leading-[32px] font-medium
                mobile:text-[16px] mobile:leading-[24px]
                leading-normal text-gray-800" id="exampleModalLabel">Pending Transaction</h5>
            </div>
            <div class="modal-body relative p-4">
                <div class="my-[48px] ">
                    <div class="flex flex-col items-center justify-center">
                        <img src="https://mweb-cdn.karousell.com/build/no-wallet-transaction-3bib3e4oh-.svg"
                             class="w-[128px] h-[128px] mt-[48px] mb-[96px]">
                        <p class="text-[14px] leading-[22px] text-[#2c2c2d]">
                            Start selling with Carousell Protection and get money in your Balance!
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

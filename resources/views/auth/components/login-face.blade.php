<div id="modal-login" class="modal-login absolute w-full bg-[rgba(0,0,0,0.3)] hidden z-40 overflow-y: scroll;" aria-hidden="true" style="padding-top: 200px;padding-bottom: 120px;">
    <div class="md:w-[472px] rounded-lg px-5 m-auto bg-white">
        <div class="border-b m-auto relative">
            <img class="w-48 py-4" src="https://mweb-cdn.karousell.com/build/carousell-logo-title-2Nnf7YFiNk.svg" alt="">
            <div class="close absolute right-0 top-0 mt-3 rounded-full cursor-pointer hover:shadow-[rgba(0, 0, 0, 0.3)]">
                <i class="fa-sharp fa-solid fa-xmark text-2xl"></i>
            </div>
        </div>
            <div>
                <button class="w-full px-6 py-2 mt-12 rounded-md bg-[#4567b2]">
                    <span class="font-semibold text-white text-lg"><i class="fa-brands fa-square-facebook"></i> Login with Facebook</span>
                </button>
            </div>
            <p class="text-center text-lg mt-4 text-[#008f79]">OR</p>
            <form action="" class="space-y-6 py-6">
                <div class="">
                    <input type="text" class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="exampleFormControlInput2"
                    placeholder="Username"/>
                    </div>
                    <div class="">
                    <input type="text" class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="exampleFormControlInput2"
                    placeholder="Password"/>
                    </div>
                    <div>
                        <select class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="exampleFormControlInput2">
                            <option>Singapore</option>
                            <option>Japan</option>
                            <option>South Korea</option>
                            <option>North Korea</option>
                        </select>
                    </div>
                    <div class="">
                    <input type="email" class="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="exampleFormControlInput2"
                    placeholder="Email"/>
                    </div>
                    <div class="flex">
                        <label class="border text-lg py-2 px-3" for="">🇸🇬 +65</label>
                        <input type="email" class="form-control block  px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none" id="exampleFormControlInput2" placeholder="Mobile phone"/>
                        <button  class="border text-lg py-3 px-2 bg-[#008e78]">
                                Verify
                        </button> 
                    </div>
                    <div class="mt-1">
                            <span class="text-[#57585a] text-lg hover:text-[#008f79]" >We verify identities via mobile numbers to ensure a safe community for you to deal in. An SMS will be sent to you. Message and data rates may apply.</span>
                    </div>
                    
                    <div class="text-center">
                        <button class="w-full px-6 py-3 rounded-md bg-[#c5c5c6] transition hover:bg-sky-600 focus:bg-[#008f79]">
                            <span class="font-semibold text-white text-lg">Sign up</span>
                        </button>
                        <div class="mt-5">
                            <span class="text-lg">Have an account?<a class="text-[#57585a] font-semibold" href="#"> Login in now</a></span>
                        </div>
                        <div class="mt-5">
                            <span class="text-ms text-[#57585a]">By signing up, you agree to Carousell’s Terms of <a>Service</a> & <a>Privacy Policy</a></span>
                        </div>
                </div>
            </form>
        </div>
    </div>
</div>